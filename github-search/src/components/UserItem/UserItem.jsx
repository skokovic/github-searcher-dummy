import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import "./UserItem.css";

export class UserItem extends PureComponent {
  render() {
    const { user } = this.props;

    return (
      <Link to={`/user/${user.login}`}>
        <article className="media useritem">
          <div className="media-left">
            <figure className="image is-128x128">
              <img
                className="is-rounded"
                src={user.avatar_url}
                alt="user_image"
              />
            </figure>
          </div>

          <div className="media-content">
            <div className="content has-text-centered">
              <p>
                <br />
                <strong>{user.login}</strong> <br />
                <strong>{user.name}</strong> from{" "}
                <strong>{user.location}</strong> <br />
                {user.html_url}
              </p>
            </div>
          </div>

          <div className="media-right">
            <p>#{user.followers}</p>
          </div>
        </article>
      </Link>
    );
  }
}

UserItem.propTypes = {
  user: PropTypes.shape({
    login: PropTypes.string.isRequired,
    avatar_url: PropTypes.string.isRequired,
    html_url: PropTypes.string.isRequired,
    name: PropTypes.string,
    location: PropTypes.string,
    email: PropTypes.string,
    followers: PropTypes.number.isRequired
  })
};
