import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import "./RepoItem.css";

export class RepoItem extends PureComponent {
  render() {
    const { repo } = this.props;
    return (
      <div className="media repoitem">
        <div className="media-content">
          <div className="content hax-text-centered">
            <p>
              <strong className="is-size-4">{repo.name}</strong> <br />
              {repo.description} <br />
              <a href={repo.html_url}>{repo.html_url}</a>
            </p>
          </div>
        </div>
        <div className="media-right text-align-right">
          <p>#{repo.stargazers_count}</p>
          <br />
          <strong>{repo.language}</strong>
        </div>
      </div>
    );
  }
}

RepoItem.propTypes = {
  repo: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    language: PropTypes.string,
    html_url: PropTypes.string.isRequired,
    stargazers_count: PropTypes.number.isRequired
  })
};
