import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import "./List.css";

export class List extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  onScrollToBottom = async e => {
    if (this.state.loading) return;
    const t = e.target;

    if (t.scrollHeight - t.scrollTop === t.clientHeight) {
      this.setState({ loading: true });
      await this.props.loadMore();
      this.setState({ loading: false });
    }
  };

  render() {
    const { items, renderItem } = this.props;

    if (!items) {
      return null;
    }

    return (
      <ul className="list-custom has-text-centered" onScroll={this.onScrollToBottom}>
        {items.map(renderItem)}
        {this.state.loading && (
          <p className="button is-primary is-loading">loading ...</p>
        )}
      </ul>
    );
  }
}

List.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  renderItem: PropTypes.func.isRequired
};
