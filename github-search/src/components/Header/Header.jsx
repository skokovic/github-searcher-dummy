import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import "./Header.css";

export class Header extends PureComponent {
  render() {
    return (
      <div className="has-text-centered">
        <h1 className="title is-size-2">{this.props.title}</h1>
      </div>
    );
  }
}

Header.propTypes = {
  title: PropTypes.string.isRequired
};
