import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { debounce } from "../../utils/debouncer";

import "./Search.css";

export class Search extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      query: ""
    };

    this.getItemsDebounced = debounce(this.search, 250);
  }

  search = () => {
    this.props.getItems(this.state.query);
  };

  queryChanged = e => {
    this.setState({ query: e.target.value }, () => this.getItemsDebounced());
  };

  render() {
    const { placeholder } = this.props;

    return (
      <div className="field">
        <div className="control">
          <input
            type="text"
            className="input is-primary"
            onChange={this.queryChanged}
            placeholder={placeholder}
          />
        </div>
      </div>
    );
  }
}

Search.propTypes = {
  getItems: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired
};
