import React, { PureComponent } from "react";
import { Header } from "../../components/Header/Header";
import { List } from "../../components/List/List";
import { Search } from "../../components/Search/Search";
import PropTypes from "prop-types";

import "./Page.css";

export class Page extends PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    getItems: PropTypes.func.isRequired,
    items: PropTypes.arrayOf(PropTypes.object),
    renderItem: PropTypes.func.isRequired,
    errorMessage: PropTypes.string
  };

  render() {
    const {
      title,
      placeholder,
      getItems,
      items,
      renderItem,
      loadMore,
      errorMessage
    } = this.props;

    return (
      <div className="columns is-mobile is-centered">
        <div className="column is-8 page">
          <Header title={title} />
          <Search getItems={getItems} placeholder={placeholder} />
          {errorMessage ? (
            <h2>{errorMessage}</h2>
          ) : (
            <List items={items} renderItem={renderItem} loadMore={loadMore} />
          )}
        </div>
      </div>
    );
  }
}
