import React, { Component } from "react";
import { searchUsers } from "../../services/UserService";
import { UserItem } from "../../components/UserItem/UserItem";
import { Page } from "../Page/Page";

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      query: "",
      errorMessage: "",
      users: []
    };
  }

  componentDidMount() {
    this.getUsers();
  }

  getUsers = async query => {
    this.setState({ query });

    if (!query) {
      return;
    }
    await searchUsers(query, this.state.page)
      .then(users => this.setState({ users }))
      .catch(e => this.setState({ errorMessage: e }));
  };

  renderItem = item => <UserItem key={item.login} user={item} />;

  loadMore = async callback => {
    const { query, page, users } = this.state;

    if (!query) return;

    await searchUsers(query, page + 1)
      .then(u => this.setState({ users: users.concat(u) }))
      .catch(e => this.setState({ errorMessage: e }));

    this.setState(prevState => ({
      page: prevState.page + 1
    }));
  };

  render() {
    const { users, errorMessage } = this.state;

    return (
      <Page
        title="GitHub Search"
        placeholder="Search users"
        getItems={this.getUsers}
        items={users}
        renderItem={this.renderItem}
        loadMore={this.loadMore}
        errorMessage={errorMessage}
      />
    );
  }
}
