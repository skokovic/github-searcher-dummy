import React, { Component } from "react";
import { getUserRepos, seachUserRepos } from "../../services/UserService";
import { RepoItem } from "../../components/RepoItem/RepoItem";
import { Page } from "../Page/Page";

export class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      query: "",
      errorMessage: "",
      repos: []
    };
  }

  componentDidMount() {
    this.getRepos();
  }

  getRepos = query => {
    const { id } = this.props.match.params;
    const { page } = this.state;
    let repos = [];

    this.setState({ query });

    if (!query) {
      repos = getUserRepos(id, page);
    } else {
      repos = seachUserRepos(id, query, page);
    }
    repos
      .then(repos => this.setState({ repos }))
      .catch(e => this.setState({ errorMessage: e.message }));
  };

  renderItem = item => <RepoItem key={item.name} repo={item} />;

  loadMore = async callback => {
    const { id } = this.props.match.params;
    const { query, page, repos } = this.state;
    let repoList = [];

    if (!query) {
      repoList = await getUserRepos(id, page + 1).catch(e =>
        this.setState({ errorMessage: e.message })
      );
    } else {
      repoList = await seachUserRepos(id, query, page + 1).catch(e =>
        this.setState({ errorMessage: e.message })
      );
    }

    this.setState({ repos: repos.concat(repoList) });

    this.setState(prevState => ({
      page: prevState.page + 1
    }));
  };

  render() {
    const { repos, errorMessage } = this.state;
    const { id } = this.props.match.params;

    return (
      <Page
        title={id}
        placeholder="Search users"
        getItems={this.getRepos}
        items={repos}
        renderItem={this.renderItem}
        errorMessage={errorMessage}
        loadMore={this.loadMore}
      />
    );
  }
}
