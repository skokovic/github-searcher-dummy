const GITHUB_API = "https://api.github.com";
const username = "skokovic";
const token = "ba214bac4033640af8d726abc26897a08d7d29c7";

const headers = new Headers({
  Authorization: "Basic " + btoa(`${username}:${token}`)
});

export const githubAPI = path => {
  return fetch(`${GITHUB_API}${path}`, { headers })
    .then(response => response.json());
};
