import { githubAPI } from "./GithubService";

const PER_PAGE = 10;

export const searchUsers = (query, page) => {
  return githubAPI(`/search/users?q=${query}&page=${page}&per_page=${PER_PAGE}`)
    .then(users => users.items)
    .then(users => Promise.all(users.map(user => getUserData(user.login))));
};

export const getUserData = user => {
  return githubAPI(`/users/${user}`);
};

export const getUserRepos = (username, page) => {
  return githubAPI(`/users/${username}/repos?page=${page}&per_page=${PER_PAGE}&sort=updated`
  );
};

export const seachUserRepos = (user, query, page) => {
  return githubAPI(`/search/repositories?q=${query}+user:${user}&page=${page}&per_page=${PER_PAGE}`)
    .then(repos => repos.items);
}
